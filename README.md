Steps to generate a Excecutable File (Windows / Linux):

    1) Install Dependencies:
        ```pip3 install -r requirements.txt```

    2) Generate executable:
        ```pyinstaller script.py --onefile```

    3) Move config folder:
        ```cp config_templates/config.ini.sample dist/```
        ```mv dist/config.ini.sample dist/config.ini```
    
    4) Complete config.ini file with your AWS credentials.

    5) Move csv folder:
        ```cp config_templates/data.csv.sample dist/```
        ```mv dist/data.csv.sample dist/data.csv```        

    6) Run Script:
        ```cd dist/```
        ```./script```
    
    Voilá!