from constants import *
import requests

class APIService(object):
    def __init__(self, api_url=ENDPOINT_MILO_MM):
        super(APIService, self).__init__()
        self.api_url = api_url
    
    def post_content_data(self, data: dict):
        # Enviamos Request a Milo API
        print('LO QUE MANDO AL BACK: ', data)
        req = requests.post(self.api_url, data=data)