import os
import configparser

config = configparser.ConfigParser()
config.read('config.ini') 

#ROOT_FOLDER = os.path.dirname(os.path.realpath(__file__))
ROOT_FOLDER = os.getcwd()
MULTIMEDIA_CONTENT_FOLDER = os.path.join(ROOT_FOLDER, 'MULTIMEDIA_CONTENT')
MULTIMEDIA_INPUT_FILE = os.path.join(ROOT_FOLDER, config['milo'].get('multimedia_input_file', 'data.csv'))
ENDPOINT_MILO_MM = config['milo'].get('endpoint', 'http://milo.local.com/admin/contents')
# BUCKET CONSTANTS
BUCKET_NAME = config['aws'].get('bucket_name', 'milo.app')
AWS_REGION = config['aws'].get('region', 'sa-east-1')
AWS_PROFILE_NAME = config['aws'].get('aws_profile', 'mrouaux')
AWS_ID = config['aws'].get('aws_access_key_id')
AWS_SECRET = config['aws'].get('aws_secret_access_key')