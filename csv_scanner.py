from dataclasses import dataclass
from dataclass_csv import DataclassReader

@dataclass
class Content:
    title: str
    description: str
    type: str
    filename: str
    url: str = None
    shipped: bool = False

class CSVScanner:
    def __init__(self, filename):
        super(CSVScanner, self).__init__()
        self.filename = filename
        self.open()
    
    def open(self):
        self._file = open(self.filename, "r+")

    def get_all(self):
        reader = DataclassReader(self._file, Content)
        yield from reader

    def close(self):
        self._file.close()

    def __exit__(self):
        self.close()
