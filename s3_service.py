from constants import *
from progressbar import progressbar
import logging
import boto3

class S3Service(object):
    def __init__(self, bucket_name=BUCKET_NAME, aws_region= AWS_REGION, aws_id = AWS_ID, aws_secret = AWS_SECRET):
        super(S3Service, self).__init__()
        self.aws_id = aws_id
        self.aws_secret = aws_secret
        self.bucket_name = bucket_name
        self.aws_region = aws_region

    def get_session(self):
        custom_session = boto3.session.Session(aws_access_key_id= self.aws_id, aws_secret_access_key= self.aws_secret, region_name= self.aws_region)
        s3_resource = custom_session.resource(service_name='s3', region_name=self.aws_region)
        return s3_resource.meta.client

    def set_progress_bar(self, filepath):
        stat_info = os.stat(filepath)
        self.progress_bar = progressbar.ProgressBar(maxval=stat_info.st_size)
    
    def update_progress_bar(self, chunk):
        self.progress_bar.update(self.progress_bar.currval + chunk)

    def finish_progress_bar(self):
        self.progress_bar.finish()

    def upĺoad(self, src, dst):
        if src is None or dst is None:
            raise Exception
        self.set_progress_bar(src)
        self.progress_bar.start() 
        self.get_session().upload_file(src, self.bucket_name, dst, Callback=self.update_progress_bar)
        dst = f'https://s3.{self.aws_region}.amazonaws.com/{self.bucket_name}/{dst}'
        self.finish_progress_bar()
        return dst



class FakeS3Service(object):
    def __init__(self, bucket_name=BUCKET_NAME, aws_region = AWS_REGION, **kwargs):
        super(FakeS3Service, self).__init__()
        self.bucket_name = bucket_name
        self.aws_region = aws_region

    def upĺoad(self, src, dst):
        dst = f'https://s3-{self.aws_region}.amazonaws.com/{self.bucket_name}/{dst}'
        logging.warning(f'Uploading with fake uploader. Source: {src} - Destination: {dst}')
        return dst        