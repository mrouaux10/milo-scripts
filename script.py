import json
import os
import shutil
import logging
from constants import *
from csv_scanner import CSVScanner, Content
from dataclasses import asdict
from s3_service import FakeS3Service, S3Service
from api_service import APIService

def _get_content_metadata(path: str):
    """
    Aca leemos el archivo .json
    """
    for filename in os.listdir(path):
        if '.json' in filename:
            filename = os.path.join(path, filename)    
            break

    with open(filename, "r+") as f:
        json_metadata = json.load(f)
    return Content(**json_metadata)

def _list_all_folders_in_folder(root_folder: str):
    list_of_folders_inside = os.listdir(root_folder)
    return list_of_folders_inside

def _save_json_to_file(content: Content, folder:str):
    """
    Actualizamos el json local
    """
    logging.info(f'metadata.json updated in folder {folder}.')
    with open(os.path.join(MULTIMEDIA_CONTENT_FOLDER, folder, "metadata.json"), "w+") as f:
        json.dump(asdict(content), f, indent = 4, ensure_ascii=False)
    
def update_multimedia_content_structure_from_csv():
    if (not os.path.exists(MULTIMEDIA_CONTENT_FOLDER)):
        os.mkdir(MULTIMEDIA_CONTENT_FOLDER) #checkear si existe antes, sino no se crea
    else:
        logging.info(f'Multimedia content folder already exists.')
    for content in CSVScanner(MULTIMEDIA_INPUT_FILE).get_all():
        # 1) Creo una carpeta por cada instancia que me devuelve el scanner con el nombre del titulo
        if (not os.path.exists(os.path.join(MULTIMEDIA_CONTENT_FOLDER, f'{content.title}'))):
            os.mkdir(os.path.join(MULTIMEDIA_CONTENT_FOLDER, f'{content.title}'))
        else:
            logging.info(f'{content.title} folder already exists.')
        # 2) Muevo el content.filename (La sirenita 1.mp4) dentro de la carpeta creada en paso 1, con el nombre del filename
        if (os.path.exists(os.path.join(ROOT_FOLDER, content.filename))):
            shutil.move(os.path.join(ROOT_FOLDER, content.filename), os.path.join(MULTIMEDIA_CONTENT_FOLDER, content.title, content.filename))
        else:
            logging.warning(f'Multimedia content file missing for content {content.title}.')
        # 3) Guardo la instancia como un diccionario en un metadata.json, dentro de la carpeta filename
        if not os.path.exists(os.path.join(MULTIMEDIA_CONTENT_FOLDER, content.title, 'metadata.json')):
            _save_json_to_file(content, content.title)


def update_remote_database_from_multimedia_content_structure():

    for folder in _list_all_folders_in_folder(MULTIMEDIA_CONTENT_FOLDER):
        metadata_content = _get_content_metadata(os.path.join(MULTIMEDIA_CONTENT_FOLDER, folder))
        # Si tiene el atributo shipped en true (ya fué actualizado, no lo vuelvo a leer)
        if metadata_content.shipped:
            continue
        # Sino lo leo y lo subo a S3
        multimedia_filepath = os.path.join(MULTIMEDIA_CONTENT_FOLDER, folder, metadata_content.filename)
        if (os.path.exists(multimedia_filepath)):
            destination_url = S3Service().upĺoad(src=multimedia_filepath, dst=metadata_content.filename)
            metadata_content.url = destination_url
        else:
            logging.warning(f"Multimedia for {metadata_content.title} not found, ommiting uploading.")
        # Enviamos el json completo a la API para guardar el Content en BD
        APIService().post_content_data(asdict(metadata_content))
        metadata_content.shipped = True
        # Actualizamos el json local con la info cargada del Excel y la url del Bucket
        _save_json_to_file(metadata_content, folder)    

def main():
    update_multimedia_content_structure_from_csv()
    update_remote_database_from_multimedia_content_structure()

if __name__ == '__main__':
    main()